import numpy
import lda
FIFA_File = file("fifa.txt")
Election_File = file("USelections.txt")
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer

X = []
Y = []
fifa_tweets = 0


for line in FIFA_File:
	fifa_tweets+=1
	try:
		line = line.strip()
		X.append(line)
	except ValueError:
		pass

election_tweets = 0
for line in Election_File:
	election_tweets+=1
	try:
		line = line.strip() 
		X.append(line)
	except ValueError:
		pass


X = numpy.array(X)
vectorizer = CountVectorizer()
X = vectorizer.fit_transform(X)

model = lda.LDA(n_topics=2, n_iter=1500, random_state=1)
model.fit(X)
probs = model.doc_topic_

TP_football, TN_football, TP_elections, TN_elections = 0,0,0,0

for i in xrange(0,fifa_tweets+1):
	if probs[i][0] > probs[i][1]:
		TP_football+=1
	else:
		TN_football+=1

for i in xrange(fifa_tweets+1,fifa_tweets+election_tweets):
	if probs[i][0] < probs[i][1]:
		TP_elections+=1
	else:
		TN_elections+=1

print "FIFA: ",TP_football, TN_football, fifa_tweets
print "Elections: ",TP_elections,TN_elections, election_tweets
