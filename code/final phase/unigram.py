import re
import math


data=open('Fifa.txt','r').readlines()
tok = []
chars_fifa = []
for i in data:
	#print i
	chars = re.split(" ", i)
	chars = chars[:-1]
	for j in range(len(chars)):
		chars[j] = chars[j].lower()
		tok.append(chars[j])
	chars_fifa.append(chars)
#print chars_fifa

#Unigrams_fifa
unigram_fifa={}
for i in tok:
    if i not in unigram_fifa:
        unigram_fifa[i] = 1
    else:
        unigram_fifa[i] +=1

res_fifa=[]
res_fifa = sorted(unigram_fifa, key=unigram_fifa.get, reverse=True)
res2_fifa=[]
res3_fifa=[]
for i in range(len(res_fifa)):
    res2_fifa.append(unigram_fifa[res_fifa[i]])
    res3_fifa.append(res2_fifa[i])

#res contains sorted unigrams and res2 contain their count
print "FIFA Top 15:"
for i in range(15):
	print res_fifa[i],res3_fifa[i]




#US Election
data=open('USelections.txt','r').readlines()
tok = []
chars_use=[]
for i in data:
	#print i
	chars = re.split(" ", i)
	chars = chars[:-1]
	for j in range(len(chars)):
		chars[j] = chars[j].lower()
		tok.append(chars[j])
	chars_use.append(chars)
	#print chars
	
#Unigrams_fifa_1
unigram_use={}
for i in tok:
    if i not in unigram_use:
        unigram_use[i] = 1
    else:
        unigram_use[i] +=1

res_use=[]
res_use = sorted(unigram_use, key=unigram_use.get, reverse=True)
res2_use=[]
res3_use=[]
for i in range(len(res_use)):
    res2_use.append(unigram_use[res_use[i]])
    res3_use.append(res2_use[i]*1.0)

print 
#res contains sorted unigrams and res2 contain their count
print "USE Top 15:"
for i in range(15):
	print res_use[i],res3_use[i]


#Super Tuesday
data=open('SuperTuesday.txt','r').readlines()
tok = []
chars_st=[]
for i in data:
	#print i
	chars = re.split(" ", i)
	chars = chars[:-1]
	for j in range(len(chars)):
		chars[j] = chars[j].lower()
		tok.append(chars[j])
	chars_st.append(chars)
	#print chars
	
#Unigrams_fifa_1
unigram_st={}
for i in tok:
    if i not in unigram_st:
        unigram_st[i] = 1
    else:
        unigram_st[i] +=1

res_st=[]
res_st = sorted(unigram_st, key=unigram_st.get, reverse=True)
res2_st=[]
res3_st=[]
for i in range(len(res_st)):
    res2_st.append(unigram_st[res_st[i]])
    res3_st.append(res2_st[i]*1.0)

print 
#res contains sorted unigrams and res2 contain their count
print "ST Top 15:"
for i in range(15):
	print res_st[i],res3_st[i]


#Smoothing left
print len(res_fifa)
print len(res_use)

#schema -> fifa->0,use->1,
print "Calculating FIFA TF"
tf = {}
for i in range(len(res_fifa)):
	if res_fifa[i] not in tf:
		num = []
		for j in range(len(chars_fifa)):
			count = 0
			#print chars_fifa[j]
			for k in chars_fifa[j]:
				#print k
				if k == res_fifa[i]:
					count += 1
			num.append(count)
		tf[res_fifa[i]] = num

print "Calculating USE TF"
for i in range(len(res_use)):
	if res_use[i] not in tf:
		num = []
		for j in range(len(chars_use)):
			count = 0
			#print chars_fifa[j]
			for k in chars_use[j]:
				#print k
				if k == res_use[i]:
					count += 1
			num.append(count)
		tf[res_use[i]] = num
	else:
		num = tf[res_use[i]]
		for j in range(len(chars_use)):
			count = 0
			#print chars_fifa[j]
			for k in chars_use[j]:
				#print k
				if k == res_use[i]:
					count += 1
			num.append(count)
		tf[res_use[i]] = num

print "Calculating ST TF"
for i in range(len(res_st)):
	if res_st[i] not in tf:
		num = []
		for j in range(len(chars_st)):
			count = 0
			#print chars_fifa[j]
			for k in chars_st[j]:
				#print k
				if k == res_st[i]:
					count += 1
			num.append(count)
		tf[res_st[i]] = num
	else:
		num = tf[res_st[i]]
		for j in range(len(chars_st)):
			count = 0
			#print chars_fifa[j]
			for k in chars_st[j]:
				#print k
				if k == res_st[i]:
					count += 1
			num.append(count)
		tf[res_st[i]] = num

fifa_len = 1902
use_len = 2200
st_len = 2021
total_len = 1902 + 2200 + 2021

print "Equalling tf length"
count = 0
for i in tf:
	count += 1
	print count,len(tf)
	a = tf[i]
	if len(a) == fifa_len:
		for j in range(use_len+st_len):
			a.append(0)
		tf[i] = a

	elif len(a) == use_len:
		for j in range(st_len):
			a.append(0)
		tf[i] = a
		a = tf[i]
		b = 0
		for j in range(fifa_len):
			a = [b] + a
		tf[i] = a

	elif len(a) == st_len:
		b = 0
		for j in range(fifa_len+use_len):
			a = [b] + a
		tf[i] = a

	elif len(a) == use_len + st_len:
		b = 0
		for j in range(fifa_len):
			a = [b] + a
		tf[i] = a

	elif len(a) == fifa_len + use_len:
		for j in range(st_len):
			a.append(0)
		tf[i] = a

	elif len(a) == fifa_len + st_len:
		for j in range(use_len):
			a.insert(fifa_len ,0)




tf.pop("")

print "Calculating IDF"

idf = {}
#print tf['obama']

for i,j in tf.iteritems():
	count1 = 0
	for k in j:
		if k!=0:
			count1 += 1
	doc_no = len(j)
	idf[i] = 1 + (math.log(doc_no/(count1*1.0))/math.log(math.e))

tfidf = {}
for i in tf:
	tfidf[i] = tf[i]
print "Calculating TFIDF"

for i ,j in tfidf.iteritems():
	b = []
	for k in range(len(j)):
		b.append(j[k] * idf[i])
	tfidf[i] = b


#Testing starts
print "Testing"
a = ['obama', 'won' ,'the', 'fucking','election']
train_score = []
for i in range(len(a)):
	train_score.append(tfidf[a[i]])


unique_pts = []
for i in range(total_len):
	a = ''
	for j in range(len(train_score)):
		a += str(train_score[j][i])
	if a not in unique_pts:
		unique_pts.append(a)

print len(unique_pts)

"""
import matplotlib.pyplot as plt1
plt1.axis([0, 100, 0, 100])		
plt1.scatter(a,b,color = 'blue')
plt1.show()
"""
#print a
#print b
#plt1.plot([1,2,3,4], [1,4,9,16], 'ro')
#plt1.axis([-0.001, 0.02, -0.001, 0.02])
#plt1.show()
#Training Done

"""
#obama won
print tfidf['obama']
print tfidf['fuck']

test_tf = [0.5,0.5]
test_idf = [1 + (math.log(2)/math.log(math.e)),1.0 ]
test_tfidf = []
for i in range(len(test_tf)):
	test_tfidf.append(  test_tf[i] * test_idf[i])
print test_tfidf
a = [0.0,  0.0009309017052426691]
b = [0.046531288679407555, 0.0024622716441620334]

def cosine_similarity(query,doc):
	dot = sum(p*q for p,q in zip(query, doc))
	q = math.sqrt(query[0] * query[0] + query[1]*query[1])
	d = math.sqrt(doc[0]*doc[0] + doc[1]*doc[1])
	return dot/(q*d)

print cosine_similarity(test_tfidf,a)
print cosine_similarity(test_tfidf,b)

"""


