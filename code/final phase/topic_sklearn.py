import nltk
import numpy
import sklearn
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm

training_file = file("trainingdata.txt")

training_data = None

X = []
Y = []


for line in training_file:
	try:
		line = line.strip()
		topic_id = line[0]
		doc = line[3:]
		#words = doc.split(" ")
		#words = filter(lambda x : x not in stop_words and not x.isdigit(),words)
		X.append(doc)
		Y.append(topic_id)
	except ValueError:
		pass
	

X = numpy.array(X)
Y = numpy.array(Y)



vectorizer = TfidfVectorizer(min_df=10,
                            max_df = 0.2,
                             sublinear_tf=True,
                             use_idf=True)
X = vectorizer.fit_transform(X)

classifier_rbf = svm.SVC(kernel='linear')
classifier_rbf.fit(X,Y)


testX = []
N = int(raw_input())
while(N>0):
	try:
		doc = raw_input()	
		testX.append(doc)
	except EOFError:
		pass
	N-=1
testX = vectorizer.transform(testX)
predict = classifier_rbf.predict(testX)
for i in predict:
	print i
